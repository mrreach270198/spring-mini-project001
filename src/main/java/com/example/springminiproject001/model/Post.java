package com.example.springminiproject001.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post {

    int id;
    String title;
    String description;
    String subreddit;
    String image;
    int vote;
    String dateTime;
    String comment;
}
