package com.example.springminiproject001.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Subreddit {
    int id;
    String title;
    String description;
}
