package com.example.springminiproject001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMiniProject001Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringMiniProject001Application.class, args);
    }

}
