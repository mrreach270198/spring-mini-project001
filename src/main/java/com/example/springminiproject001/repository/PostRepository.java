package com.example.springminiproject001.repository;

import com.example.springminiproject001.model.Post;
import com.example.springminiproject001.model.Subreddit;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository {

    @Insert("insert into tb_post (title, description, subreddit, image, vote, date_time, comment) values (#{post.title},#{post.description},#{post.subreddit},#{post.image},#{post.vote},#{post.dateTime},#{post.comment})")
    public Boolean insert(@Param("post")Post post);

    @Select("select *from tb_post order by post_id desc")
    @Results(id = "mappingPost", value = {
            @Result(property = "id", column = "post_id"),
            @Result(property = "dateTime", column = "date_time")
    })
    public List<Post> getAllPost();

    @Insert("insert into tb_subreddit (sub_title, sub_description) values (#{subreddit.title},#{subreddit.description})")
    public Boolean insertSubreddit(@Param("subreddit")Subreddit subreddit);

    @Select("select *from tb_subreddit")
    @Results(id = "mappingSubreddit", value = {
            @Result(property = "title", column = "sub_title"),
            @Result(property = "description", column = "sub_description"),
    })
    public List<Subreddit> getAllSubreddit();

    @Update("update tb_post set vote=#{vote}+#{plus} where vote=#{vote}")
    public Boolean updateUpVote(@Param("vote") int vote, @Param("plus") int plus);

    @Update("update tb_post set vote=#{vote}-#{minus} where vote=#{vote}")
    public Boolean updateDownVote(@Param("vote") int vote, @Param("minus") int minus);

    @Delete("delete from tb_post where post_id=#{id}")
    public Boolean deletePost(@Param("id") int id);
}
