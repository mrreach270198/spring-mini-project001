package com.example.springminiproject001.controller;

import com.example.springminiproject001.model.Post;
import com.example.springminiproject001.model.Subreddit;
import com.example.springminiproject001.service.imp.PostServiceImp;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class PostController {

    PostRestController postRestController;

    @Autowired
    public PostController(PostRestController postRestController) {
        this.postRestController = postRestController;
    }

    @GetMapping("/")
    public String home(Model model){
        model.addAttribute("post", PostServiceImp.postList);
        postRestController.getAllPost();
        model.addAttribute("subreddit", PostServiceImp.subredditList);
        postRestController.getAllSubreddit();
        return "home";
    }

    @GetMapping("/post/create")
    public String createPost(){
        return "create-post";
    }

    @GetMapping("/subreddit/create")
    public String createSubreddit(){

        return "create-subreddit";
    }

    @GetMapping("/view/post")
    public String viewPage(){

        return "view-page";
    }

    @PostMapping("/post")
    public String post(@RequestParam("file")MultipartFile file, @ModelAttribute Post post){
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        Date date = new Date();

        try {

            String fileName = file.getOriginalFilename();
            Path path = Paths.get("src/main/resources/files/"+ fileName);
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);

            post.setImage(fileName);
            post.setVote(0);
            post.setDateTime(formatter.format(date));
            post.setComment("0");

            postRestController.createPost(post);
            System.out.println("Data from form : "+post);
        }catch (IOException e){
            e.printStackTrace();
        }
        return "redirect:/";
    }

    @PostMapping("/subreddit")
    public String createSubreddit(@ModelAttribute Subreddit subreddit){
        postRestController.createSubreddit(subreddit);
        System.out.println("subreddit create success");
        return "redirect:/";
    }

    @GetMapping("/up/{vote}")
    public String getUpVote(@PathVariable("vote") int vote){
        postRestController.updateUpVote(vote, 1);
        System.out.println("Up vote : "+ vote);
        return "redirect:/";
    }

    @GetMapping("/down/{vote}")
    public String getDownVote(@PathVariable("vote") int vote){
        postRestController.updateDownVote(vote, 1);
        System.out.println("Down vote : "+ vote);
        return "redirect:/";
    }

    @GetMapping("/delete/{id}")
    public String deletePost(@PathVariable("id") int id){
        postRestController.deletePost(id);
        return "redirect:/";
    }
}
