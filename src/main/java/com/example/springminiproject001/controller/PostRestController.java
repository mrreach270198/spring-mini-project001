package com.example.springminiproject001.controller;

import com.example.springminiproject001.model.Post;
import com.example.springminiproject001.model.Subreddit;
import com.example.springminiproject001.repository.PostRepository;
import com.example.springminiproject001.service.PostService;
import com.example.springminiproject001.service.imp.PostServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PostRestController {

    private final PostService postService;
    private PostRepository postRepository;

    @Autowired
    public void setPostRepository(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Autowired
    public PostRestController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping
    public String createPost(Post post){
        if (postService.createPost(post)){
            return "Insert success!";
        }else {
            return "insert fail";
        }
    }

    @GetMapping("/get/post")
    public List<Post> getAllPost(){
        PostServiceImp.postList.clear();
        PostServiceImp.postList.addAll(postService.readAllPost());
        System.out.println(PostServiceImp.postList);
        return postService.readAllPost();
    }

    @PostMapping("/create/subreddit")
    public String createSubreddit(Subreddit subreddit){
        if (postService.createSubreddit(subreddit)){
            return "Subreddit create success";
        }else {
            return "Subreddit create fail";
        }
    }

    @GetMapping("/get/subreddit")
    public List<Subreddit> getAllSubreddit(){
        PostServiceImp.subredditList.clear();
        PostServiceImp.subredditList.addAll(postService.getAllSubreddit());
        return postService.getAllSubreddit();
    }

    @PutMapping("/up")
    public Boolean updateUpVote(int vote, int plus){
        if (postService.updateUpVote(vote, plus)){
            return true;
        }else {
            return false;
        }
    }

    @PutMapping("/down")
    public Boolean updateDownVote(int vote, int minus){
        if (postService.updateDownVote(vote, minus)){
            return true;
        }else {
            return false;
        }
    }

    @DeleteMapping("/delete")
    public Boolean deletePost(int id){
        if (postService.deletePost(id)){
            return true;
        }else {
            return false;
        }
    }
}
