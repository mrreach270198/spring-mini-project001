package com.example.springminiproject001.service;

import com.example.springminiproject001.model.Post;
import com.example.springminiproject001.model.Subreddit;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

public interface PostService {

    Boolean createPost(Post post);
    List<Post> readAllPost();
    Boolean createSubreddit(Subreddit subreddit);
    List<Subreddit> getAllSubreddit();
    public Boolean updateUpVote(int vote, int plus);
    public Boolean updateDownVote(int vote, int minus);
    public Boolean deletePost(int id);
}
