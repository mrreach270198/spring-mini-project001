package com.example.springminiproject001.service.imp;

import com.example.springminiproject001.model.Post;
import com.example.springminiproject001.model.Subreddit;
import com.example.springminiproject001.repository.PostRepository;
import com.example.springminiproject001.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostServiceImp implements PostService {

    private PostRepository postRepository;

    public static ArrayList<Post> postList = new ArrayList<>();
    public static ArrayList<Subreddit> subredditList = new ArrayList<>();

    @Autowired
    public PostServiceImp(PostRepository postRepository) {

        this.postRepository = postRepository;
    }

    @Override
    public Boolean createPost(Post post) {
        return postRepository.insert(post);
    }

    @Override
    public List<Post> readAllPost() {
        return postRepository.getAllPost();
    }

    @Override
    public Boolean createSubreddit(Subreddit subreddit) {
        return postRepository.insertSubreddit(subreddit);
    }

    @Override
    public List<Subreddit> getAllSubreddit() {
        return postRepository.getAllSubreddit();
    }

    @Override
    public Boolean updateUpVote(int vote, int plus) {
        return postRepository.updateUpVote(vote, plus);
    }

    @Override
    public Boolean updateDownVote(int vote, int minus) {
        return postRepository.updateDownVote(vote, minus);
    }

    @Override
    public Boolean deletePost(int id) {
        return postRepository.deletePost(id);
    }
}
